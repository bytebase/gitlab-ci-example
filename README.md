# gitlab-ci-example

[GitHub equivalent](https://github.com/bytebase/github-action-example)

Sample GitLab CI to call Bytebase API to coordinate the schema migration in Bytebase with the GitLab MR workflow. A typical workflow works like this:

1. Create MR containing both code change and schema migration for review.
1. MR approved.
1. Rollout the schema migration.
1. Merge the PR and kicks of the pipeline to release the application.
